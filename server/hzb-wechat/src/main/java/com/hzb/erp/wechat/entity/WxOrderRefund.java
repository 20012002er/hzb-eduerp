package com.hzb.erp.wechat.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 订单实体
 * </p>
 *
 * @author Ryan
 */
@Data
@ApiModel(value = "订单实体", description = "订单实体")
public class WxOrderRefund implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "退款编号")
    private String refundSn;

    @ApiModelProperty(value = "订单ID")
    private Long orderId;

    @ApiModelProperty(value = "退款金额")
    private BigDecimal refundMoney;
}
