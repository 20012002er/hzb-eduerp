package com.hzb.erp.wechat.pojo.wxMenu;

import com.hzb.erp.wechat.entity.WxMenuEntity;
import lombok.Data;

import java.util.List;

/**
 * <p> 微信菜单 </p>
 *
 * @author Ryan 541720500@qq.com
 */
@Data
public class WxMenuDTO {
    private String id;
    private String name;
    private String type;
    private String key;
    private String url;
    private String appid;
    private String pagepath;
    private String mediaId;
    private String articleId;

    private List<WxMenuEntity> children;
}
