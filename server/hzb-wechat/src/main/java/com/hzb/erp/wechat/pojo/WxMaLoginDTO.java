package com.hzb.erp.wechat.pojo;

import lombok.Data;

/**
 * <p> 微信小程序自定义登录类 </p>
 *
 * @author Ryan 541720500@qq.com
 */
@Data
public class WxMaLoginDTO {
    private String code;
    private String nickName;
    private String avatarUrl;
    private Integer gender;
    private String city;
    private String province;
    private String country;
}
