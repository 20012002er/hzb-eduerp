package com.hzb.erp.wechat.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 微信公众号配置
 * 20220313 Ryan 可从配置文件读取，也可以从数据库读取
 */
@Data
@Component
@ConfigurationProperties(prefix = "wxconfig")
public class WxMpConfig {
    /**
     * 微信公众号配置名称
     */
    private String name;
    /**
     * 设置微信公众号的appid
     */
    private String appId;

    /**
     * 设置微信公众号的app secret
     */
    private String secret;

    /**
     * 设置微信公众号的token
     */
    private String token;

    /**
     * 设置微信公众号的EncodingAESKey
     */
    private String aesKey;


    /**
     * 微信支付商户号
     */
    private String mchId;

    /**
     * 微信支付商户密钥
     */
    private String mchKey;

    /**
     * 服务商模式下的子商户公众账号ID，普通模式请不要配置
     */
    private String subAppId;

    /**
     * 服务商模式下的子商户号，普通模式请不要配置
     */
    private String subMchId;

    /**
     * apiclient_cert.p12文件的绝对路径，或者如果放在项目中，请以classpath:开头指定
     */
    private String keyPath;


}
