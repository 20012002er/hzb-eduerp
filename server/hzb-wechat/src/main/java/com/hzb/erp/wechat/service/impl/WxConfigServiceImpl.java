package com.hzb.erp.wechat.service.impl;

import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.service.WxPayService;
import com.hzb.erp.wechat.service.WechatService;
import com.hzb.erp.wechat.service.WxConfigService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p> 微信配置服务实现类 </p>
 *
 * @author Ryan 541720500@qq.com
 */
@Service
@AllArgsConstructor
@Slf4j
public class WxConfigServiceImpl implements WxConfigService {
    private final WxPayService wxPayService;
    private final WechatService wechatService;

    @Override
    public void bindPaymentConfig() {
        WxPayConfig payConfig = wechatService.getPayConfig();
        log.info("微信支付服务参数：");
        log.info(String.valueOf(payConfig));
        if(payConfig == null) {
           throw new RuntimeException("缺少微信支付配置,请检查");
        }
        // 可以指定是否使用沙箱环境
        payConfig.setUseSandboxEnv(false);
        wxPayService.setConfig(payConfig);
    }
}
