package com.hzb.erp.wechat.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hzb.erp.wechat.entity.WxMenuEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 微信菜单mapper
 */
@Mapper
public interface WxMenuMapper extends BaseMapper<WxMenuEntity> {

}




