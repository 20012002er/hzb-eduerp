package com.hzb.erp.wechat.entiry;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 订单实体
 * </p>
 *
 * @author Ryan
 */
@Data
@ApiModel(value = "订单实体", description = "订单实体")
public class WxOrder implements Serializable {

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "订单号")
    private String sn;

    @ApiModelProperty(value = "订单金额")
    private BigDecimal orderMoney;

    @ApiModelProperty(value = "实际支付金额")
    private BigDecimal payMoney;

    @ApiModelProperty(value = "备注")
    private String remark;

}
