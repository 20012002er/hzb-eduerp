package com.hzb.erp.wechat.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 微信小程序相关配置 与yml对应
 */
@Data
@Component
@ConfigurationProperties(prefix = "miniappconfig")
public class MiniAppConfig {

    /**
     * 微信公众号配置名称
     */
    private String name;

    private String appId;
    private String secret;
    private String token;
    private String aesKey;

}
