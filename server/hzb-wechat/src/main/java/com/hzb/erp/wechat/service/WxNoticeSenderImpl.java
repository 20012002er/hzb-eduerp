package com.hzb.erp.wechat.service;

import com.hzb.erp.configuration.SystemConfig;
import com.hzb.erp.sysservice.notification.NoticeCodeEnum;
import com.hzb.erp.sysservice.notification.WxNoticeSender;
import com.hzb.erp.sysservice.notification.bo.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * @author Ryan 541720500@qq.com
 * description 微信发送器
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class WxNoticeSenderImpl implements WxNoticeSender {

    private final WechatService wechatService;
    private final SystemConfig systemConfig;

    /**
     * 发送消息
     * 模板的选择需要进行原则上是和模板消息体内容大体一致即可。
     */
    @Override
    public Boolean send(String openId, String templateId, NoticeBO param, NoticeCodeEnum code) {

        String studentUrl = systemConfig.getDomain() + "/s";
        String teacherUrl = systemConfig.getDomain() + "/t";

        if (StringUtils.isBlank(openId)) {
            return null;
        }

        WxMpTemplateMessage templateMessage = WxMpTemplateMessage.builder()
                .toUser(openId)
                .templateId(templateId)
                .build();

        switch (code) {
            case STUDENT_LESSON_START:
//             公众号消息模板搜标题：上课提醒通知，行业：教育 - 培训，编号：OPENTM206931431
//                详细内容
//            {{first.DATA}}
//            课程名称：{{keyword1.DATA}}
//            上课时间：{{keyword2.DATA}}
//            上课地点：{{keyword3.DATA}}
//            {{remark.DATA}}

                LessonStartBO bo = (LessonStartBO) param;
                templateMessage
                        .addData(new WxMpTemplateData("first", bo.getContent()))
                        .addData(new WxMpTemplateData("keyword1", bo.getCourseName()))
                        .addData(new WxMpTemplateData("keyword2", bo.getDate() + " " + bo.getStartTime()))
                        .addData(new WxMpTemplateData("keyword3", bo.getClassroom()))
                        .addData(new WxMpTemplateData("remark", StringUtils.isNotBlank(bo.getRemark()) ? bo.getRemark() : "请按时到课。"))
                        .setUrl(studentUrl);
                break;

            case TEACHER_LESSON_START:
//             公众号消息模板搜标题：上课提醒通知，行业：教育 - 培训，编号：OPENTM206931431
//                详细内容
//            {{first.DATA}}
//            课程名称：{{keyword1.DATA}}
//            上课时间：{{keyword2.DATA}}
//            上课地点：{{keyword3.DATA}}
//            {{remark.DATA}}

                LessonStartBO bo1 = (LessonStartBO) param;
                templateMessage
                        .addData(new WxMpTemplateData("first", bo1.getContent()))
                        .addData(new WxMpTemplateData("keyword1", bo1.getCourseName()))
                        .addData(new WxMpTemplateData("keyword2", bo1.getDate() + " " + bo1.getStartTime()))
                        .addData(new WxMpTemplateData("keyword3", bo1.getClassroom()))
                        .addData(new WxMpTemplateData("remark", StringUtils.isNotBlank(bo1.getRemark()) ? bo1.getRemark() : "请按时到课。"))
                        .setUrl(teacherUrl);
                break;

            case STUDENT_SIGN:

//             公众号消息模板搜标题：消课完成提醒，行业：教育 - 培训，编号：OPENTM416236454
//            {{first.DATA}}
//            课程名称：{{keyword1.DATA}}
//            剩余课时：{{keyword2.DATA}}
//            上课时间：{{keyword3.DATA}}
//            消课情况：{{keyword4.DATA}}
//            {{remark.DATA}}

                StudentSignBO bo2 = (StudentSignBO) param;
                templateMessage
                        .addData(new WxMpTemplateData("first", bo2.getContent()))
                        .addData(new WxMpTemplateData("keyword1", bo2.getLessonTitle()))
                        .addData(new WxMpTemplateData("keyword2", bo2.getSubject()))
                        .addData(new WxMpTemplateData("keyword3", bo2.getDate() + " " + bo2.getTime()))
                        .addData(new WxMpTemplateData("keyword4", bo2.getState()))
                        .addData(new WxMpTemplateData("remark", StringUtils.isNotBlank(bo2.getRemark()) ? bo2.getRemark() : "在老师评价后会奖励积分，您也可以在上课记录里给老师评价呦。"))
                        .setUrl(studentUrl);
                break;

            case STUDENT_LESSON_ONCHANGE:
//             公众号消息模板搜标题：课程状态变更通知，行业：教育 - 培训，编号：OPENTM207707252
//            {{first.DATA}}
//            课程名称：{{keyword1.DATA}}
//            课程说明：{{keyword2.DATA}}
//            {{remark.DATA}}

                LessonChangeBO bo31 = (LessonChangeBO) param;
                templateMessage
                        .addData(new WxMpTemplateData("first", bo31.getContent()))
                        .addData(new WxMpTemplateData("keyword1", bo31.getLessonTitle()))
                        .addData(new WxMpTemplateData("keyword2", "课程状态变为" + bo31.getNewState()))
                        .addData(new WxMpTemplateData("remark", "原上课时间" + bo31.getDate() + " " + bo31.getStartTime() + "。" + (StringUtils.isNotBlank(bo31.getRemark()) ? bo31.getRemark() : "")))
                        .setUrl(studentUrl);
                break;

            case TEACHER_LESSON_ONCHANGE:

//             公众号消息模板搜标题：课程状态变更通知，行业：教育 - 培训，编号：OPENTM207707252
//            {{first.DATA}}
//            课程名称：{{keyword1.DATA}}
//            课程说明：{{keyword2.DATA}}
//            {{remark.DATA}}

                LessonChangeBO bo3 = (LessonChangeBO) param;
                templateMessage
                        .addData(new WxMpTemplateData("first", bo3.getContent()))
                        .addData(new WxMpTemplateData("keyword1", bo3.getLessonTitle()))
                        .addData(new WxMpTemplateData("keyword2", "课程状态变为" + bo3.getNewState()))
                        .addData(new WxMpTemplateData("remark", "原上课时间" + bo3.getDate() + " " + bo3.getStartTime() + "。" + (StringUtils.isNotBlank(bo3.getRemark()) ? bo3.getRemark() : "")))
                        .setUrl(teacherUrl);
                break;

            case STUDENT_NEW_CONTRACT:

//             公众号消息模板搜标题：报名结果通知，行业：教育 - 培训，编号：OPENTM411026400
//            {{first.DATA}}
//            报名内容：{{keyword1.DATA}}
//            报名结果：{{keyword2.DATA}}
//            {{remark.DATA}}

                NewContractBO bo4 = (NewContractBO) param;
                templateMessage
                        .addData(new WxMpTemplateData("first", bo4.getContent()))
                        .addData(new WxMpTemplateData("keyword1", bo4.getCourseName()))
                        .addData(new WxMpTemplateData("keyword2", "已报名成功"))
                        .addData(new WxMpTemplateData("remark", "课时数：" + bo4.getLessonCount() + ", 实付金额：" + bo4.getAmount() + "。" + (StringUtils.isNotBlank(bo4.getRemark()) ? bo4.getRemark() : "")))
                        .setUrl(studentUrl);
                break;

            case STUDENT_LESSON_COUNT_LESS:
//             公众号消息模板搜标题：缴费提醒，行业：教育 - 培训，编号：OPENTM405831457
//            {{first.DATA}}
//            学员名称：{{keyword1.DATA}}
//            剩余课时：{{keyword2.DATA}}
//            {{remark.DATA}}
                LessonNotEnoughBO bo51 = (LessonNotEnoughBO) param;
                templateMessage
                        .addData(new WxMpTemplateData("first", bo51.getContent()))
                        .addData(new WxMpTemplateData("keyword1", bo51.getStudentName()))
                        .addData(new WxMpTemplateData("keyword2", bo51.getLessonCount()))
                        .addData(new WxMpTemplateData("remark", "该课程过期时间" + bo51.getExpireDate() + "。" + (StringUtils.isNotBlank(bo51.getRemark()) ? bo51.getRemark() : "")))
                        .setUrl(studentUrl);
                break;

            case TEACHER_STUDENT_LESSONLESS:

//             公众号消息模板搜标题：缴费提醒，行业：教育 - 培训，编号：OPENTM405831457
//            {{first.DATA}}
//            学员名称：{{keyword1.DATA}}
//            剩余课时：{{keyword2.DATA}}
//            {{remark.DATA}}
                LessonNotEnoughBO bo5 = (LessonNotEnoughBO) param;
                templateMessage
                        .addData(new WxMpTemplateData("first", bo5.getContent()))
                        .addData(new WxMpTemplateData("keyword1", bo5.getStudentName()))
                        .addData(new WxMpTemplateData("keyword2", bo5.getLessonCount()))
                        .addData(new WxMpTemplateData("remark", "该课程过期时间" + bo5.getExpireDate() + "。" + (StringUtils.isNotBlank(bo5.getRemark()) ? bo5.getRemark() : "")))
                        .setUrl(teacherUrl);
                break;

            case TEACHER_STUDENT_LEAVE:

//             公众号消息模板搜标题：学生请假通知，行业：教育 - 培训，编号：OPENTM415433846
//            {{first.DATA}}
//            学生姓名：{{keyword1.DATA}}
//            课程名称：{{keyword2.DATA}}
//            原上课时间：{{keyword3.DATA}}
//            {{remark.DATA}}

                StudentLeaveBO bo6 = (StudentLeaveBO) param;
                templateMessage
                        .addData(new WxMpTemplateData("first", bo6.getContent()))
                        .addData(new WxMpTemplateData("keyword1", bo6.getStudentName()))
                        .addData(new WxMpTemplateData("keyword2", bo6.getLessonTitle()))
                        .addData(new WxMpTemplateData("keyword3", bo6.getDate() + " " + bo6.getStartTime()))
                        .addData(new WxMpTemplateData("remark", "请假原因:" + ( StringUtils.isBlank(bo6.getSubject()) ? "无" : bo6.getSubject()) + "。" + (StringUtils.isNotBlank(bo6.getRemark()) ? bo6.getRemark() : "")))
                        .setUrl(teacherUrl);

                break;

            case STUDENT_NEW_ORDER:

//             公众号消息模板搜标题：订单完成通知，行业：IT科技 - IT软件与服务，编号：OPENTM204987069
//            {{first.DATA}}
//            订单编号：{{keyword1.DATA}}
//            完成时间：{{keyword2.DATA}}
//            {{remark.DATA}}

                NewOrderBO bo7 = (NewOrderBO) param;
                templateMessage
                        .addData(new WxMpTemplateData("first", bo7.getContent()))
                        .addData(new WxMpTemplateData("keyword1", bo7.getOrderSn()))
                        .addData(new WxMpTemplateData("keyword2", bo7.getOrderTime()))
                        .addData(new WxMpTemplateData("remark", StringUtils.isNotBlank(bo7.getRemark()) ? bo7.getRemark() : "祝您学习愉快！"))
                        .setUrl(studentUrl);
                break;

            case TEACHER_NEW_ORDER:

//             公众号消息模板搜标题：订单完成通知，行业：IT科技 - IT软件与服务，编号：OPENTM204987069
//            {{first.DATA}}
//            订单编号：{{keyword1.DATA}}
//            完成时间：{{keyword2.DATA}}
//            {{remark.DATA}}

                NewOrderBO bo8 = (NewOrderBO) param;
                templateMessage
                        .addData(new WxMpTemplateData("first", bo8.getContent()))
                        .addData(new WxMpTemplateData("keyword1", bo8.getOrderSn()))
                        .addData(new WxMpTemplateData("keyword2", bo8.getOrderTime()))
                        .addData(new WxMpTemplateData("remark", StringUtils.isNotBlank(bo8.getRemark()) ? bo8.getRemark() : "请登录查看"))
                        .setUrl(teacherUrl);
                break;

            case TEACHER_NEW_APPOINTMENT:

//             公众号消息模板搜标题：课程状态变更通知， 行业：教育 - 培训，编号：OPENTM207707252
//            {{first.DATA}}
//            课程名称：{{keyword1.DATA}}
//            课程说明：{{keyword2.DATA}}
//            {{remark.DATA}}

                LessonChangeBO bo9 = (LessonChangeBO) param;
                templateMessage
                        .addData(new WxMpTemplateData("first", bo9.getContent()))
                        .addData(new WxMpTemplateData("keyword1", bo9.getLessonTitle()))
                        .addData(new WxMpTemplateData("keyword2", "预约成功"))
                        .addData(new WxMpTemplateData("remark", StringUtils.isNotBlank(bo9.getRemark()) ? bo9.getRemark() : "请登录查看"))
                        .setUrl(teacherUrl);
                break;

            case STUDENT_LESSON_EVALUATED:

//            公众号消息模板搜标题：点评结果通知， 行业：教育 - 培训，编号：OPENTM416648520
//            {{first.DATA}}
//            学生姓名：{{keyword1.DATA}}
//            课程名称：{{keyword2.DATA}}
//            点评老师：{{keyword3.DATA}}
//            老师点评：{{keyword4.DATA}}
//            {{remark.DATA}}

                LessonEvaluateBO bo10 = (LessonEvaluateBO) param;
                templateMessage
                        .addData(new WxMpTemplateData("first", ""))
                        .addData(new WxMpTemplateData("keyword1", bo10.getStudentName()))
                        .addData(new WxMpTemplateData("keyword2", bo10.getLessonTitle()))
                        .addData(new WxMpTemplateData("keyword3", bo10.getTeacherName()))
                        .addData(new WxMpTemplateData("keyword4", bo10.getContent()))
                        .addData(new WxMpTemplateData("remark",bo10.getScoreInfo() + "。" + (StringUtils.isNotBlank(bo10.getRemark()) ? bo10.getRemark() : "")))
                        .setUrl(studentUrl);
                break;

            default:
                throw new RuntimeException("未知NoticeBO类型");
        }

        log.info("=======微信发送模板消息===========");
        log.info("模板Id：{} 参数：{} openId：{}", templateId, param, openId);

        try {
            wechatService.sendTemplateMsg(templateMessage);
        } catch (WxErrorException e) {
            log.error("微信模板消息发送异常：" + e.getMessage());
            return false;
        }
        return true;
    }

}
