package com.hzb.erp.wechat.pojo.wxMenu;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.chanjar.weixin.common.api.WxConsts;

/**
 * <p> 微信菜单类型枚举</p>
 *
 * @author Ryan 541720500@qq.com
 * @date 20220727
 */
@AllArgsConstructor
@Getter
public enum WxMenuTypeEnum {
    /**
    * 微信菜单类型
    * */
    VIEW("view", "打开连接"),
    CLICK("click", "点击事件"),
    MINIPROGRAM("miniprogram", "打开小程序"),
    SCANCODE_WAITMSG("scancode_waitmsg", "扫码带提示"),
    SCANCODE_PUSH("scancode_push", "扫码推事件"),
    MEDIA_ID("media_id", "发送图片"),
    ARTICLE_VIEW_LIMITED("article_view_limited", "发送发布后的图文消息"),
    SEND_TEXT("send_text", "发送给您文字"),
    ARTICLE_ID("article_id", "发送发布后的图文消息");

    private final String label;
    private final String desc;
}
