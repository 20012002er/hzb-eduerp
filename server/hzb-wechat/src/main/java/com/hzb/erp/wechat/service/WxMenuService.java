package com.hzb.erp.wechat.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzb.erp.wechat.entity.WxMenuEntity;
import me.chanjar.weixin.common.bean.menu.WxMenu;

import java.util.List;

/**
 * <p>
 * 微信菜单配置表 服务类
 * </p>
 *
 * @author Ryan
 */
public interface WxMenuService extends IService<WxMenuEntity> {

    /**
     * 列表
     * */
    List<WxMenuEntity> menuList();

    /**
    * 保存
    * */
    boolean menuSave(List<WxMenuEntity> wxMenus);

    /**
     * 获取待发布的菜单
     * */
    WxMenu getProdMenu();
}
