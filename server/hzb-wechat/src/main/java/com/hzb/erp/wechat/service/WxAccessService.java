package com.hzb.erp.wechat.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hzb.erp.common.entity.WxAccess;
import com.hzb.erp.wechat.service.pojo.WxAccessParamDTO;
import me.chanjar.weixin.common.bean.WxOAuth2UserInfo;
import me.chanjar.weixin.mp.bean.result.WxMpUser;

/**
 * <p>
 * 微信登录记录表 服务类
 * </p>
 *
 * @author Ryan
 */
public interface WxAccessService extends IService<WxAccess> {

    WxAccess getByOpenId(String openid);

    /**
     * 处理关注
     */
    Boolean subscribe(WxMpUser userWxInfo);

    /**
     * 处理取消关注
     */
    Boolean unsubscribe(String openid);

    /**
     * 获取或记录登录者
     * @param user 用户信息
     * @param from 从wx公众号注册 从mp小程序注册
     */
    WxAccess getOrSaveRecord(WxOAuth2UserInfo user, String from);

    /**
    * 分页列表
    * */
    IPage<WxAccess> getList(WxAccessParamDTO param);
}
