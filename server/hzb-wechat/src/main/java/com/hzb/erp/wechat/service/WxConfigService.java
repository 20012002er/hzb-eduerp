package com.hzb.erp.wechat.service;

/**
 * <p> 微信配置服务 </p>
 *
 * @author Ryan 541720500@qq.com
 */
public interface WxConfigService {

    /**
    * 绑定微信支付参数
    * */
    void bindPaymentConfig();

}
