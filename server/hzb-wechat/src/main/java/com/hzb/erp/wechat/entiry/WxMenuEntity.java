package com.hzb.erp.wechat.entiry;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 微信菜单配置实体类
 *
 * @author Ryan 541720500@qq.com
 * @since 2022-07-27 15:12:02
 */
@Data
@NoArgsConstructor
@TableName("wx_menu")
public class WxMenuEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @TableId
    private Long id;
    /**
     * 上级id
     */
    @ApiModelProperty(value = "上级id")
    private Long pid;
    /**
     * 菜单名
     */
    @ApiModelProperty(value = "菜单名")
    private String name;
    /**
     * 菜单URL
     */
    @ApiModelProperty(value = "菜单URL")
    private String url;
    /**
     * 菜单类型
     */
    @ApiModelProperty(value = "菜单类型")
    private String type;
    /**
     * 事件KEY
     */
    @ApiModelProperty(value = "事件KEY")
    private String keyCode;
    /**
     * 小程序APPID
     */
    @ApiModelProperty(value = "小程序APPID")
    private String appid;
    /**
     * 小程序页面地址
     */
    @ApiModelProperty(value = "小程序页面地址")
    private String pagepath;
    /**
     * 素材media_id
     */
    @ApiModelProperty(value = "素材media_id")
    private String mediaId;
    /**
     * 文章article_id
     */
    @ApiModelProperty(value = "文章article_id")
    private String articleId;

    /**
     * 发送文字
     */
    @ApiModelProperty(value = "发送文字")
    private String sendText;

    /**
     * 编辑人
     */
    @ApiModelProperty(value = "编辑人")
    private Long editor;
    /**
     * 编辑时间
     */
    @ApiModelProperty(value = "编辑时间")
    private LocalDateTime editTime;

    @TableField(exist = false)
    private List<WxMenuEntity> children;
}