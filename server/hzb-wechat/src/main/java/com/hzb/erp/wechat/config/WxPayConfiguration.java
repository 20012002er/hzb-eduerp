package com.hzb.erp.wechat.config;

import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;
import com.hzb.erp.wechat.service.WechatService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 微信支付bean
 */
@Configuration
@ConditionalOnClass(WxPayService.class)
@Slf4j
public class WxPayConfiguration {

  /**
  * 微信支付参数在请求时获取病赋值
  * */
  @Bean
  @ConditionalOnMissingBean
  public WxPayService wxPayService() {
    return new WxPayServiceImpl();
  }
}
