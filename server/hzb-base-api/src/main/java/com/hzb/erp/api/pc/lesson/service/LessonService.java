package com.hzb.erp.api.pc.lesson.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hzb.erp.api.pc.lesson.entity.Lesson;
import com.hzb.erp.api.pc.lesson.pojo.*;
import com.hzb.erp.api.pc.student.entity.Student;
import com.hzb.erp.common.entity.Staff;
import com.hzb.erp.common.enums.SignStateEnum;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课次表 服务类
 * </p>
 *
 * @author 541720500@qq.com
 */
public interface LessonService extends IService<Lesson> {

    /**
     * 带分页的列表
     *
     * @param param 参数
     */
    IPage<LessonVO> getList(LessonParamDTO param);

    /**
     * 获取老师列表
     */
    List<Staff> getTeachers(Lesson lesson);

    /**
     * 获取学生列表
     */
    List<Student> getStudents(Lesson lesson);

    /**
     * 不带分页的全部
     *
     * @param param 参数
     */
    List<LessonVO> getAll(LessonParamDTO param);

    /**
     * 通过编排计划删除课表
     */
    Boolean deleteBySchedule(List<Long> ids);

    /**
     * 课时费统计
     * 只包含已停课的，不包含未上课的
     */
    IPage<LessonTeacherStatsVO> statsByTeachers(LessonParamDTO param);

    /**
     * 消课数统计
     */
    IPage<LessonCountDecreaseStatsVO> statisDecLesson(LessonParamDTO param);

    /**
     * 删除
     */
    Boolean deleteLesson(List<Long> ids);

    /**
     * 停课
     */
    Boolean stopLesson(List<Long> ids);

    LessonVO getInfo(Long id);

    /**
     * 添加或更新单个课程
     */
    Integer saveOrUpdateByDTO(LessonSaveDTO dto);

    /**
    * 批量生成课程
    * */
    Integer batchSaveByDTO(LessonBatchSaveDTO dto);

    /**
     * 快速创建课次，选择学生即可开课，不用选择班级
     */
    Integer createQuickly(LessonSaveQuicklyDTO dto, Long creatorId);

    Boolean reopenLesson(List<Long> ids);

    /**
     * 学生签到
     */
    Boolean studentSign(Long lessonId, Long studentId, SignStateEnum state);

    /**
     * 点名
     */
//    Boolean rollCall(Long teacherId, Long lessonId, Long studentId, SignStateEnum state);

    /**
     * 批量点名
     */
    Boolean rollCallBatch(Long teacherId, List<LessonSignSaveDTO> signData);

    /**
     * 获取现在签到的状态
     */
    SignStateEnum getSignStateByNow(Long lessonId);

    /**
     * 生成学生的课次记录
     *
     * @param minutes 查询的分钟范围
     */
//    void generateLessonStudent(Integer minutes);

    /**
     * 结课逻辑
     */
    void closeLesson();

    /**
     * 导出统计
     */
    void exportStatisData(LessonParamDTO param);

    /**
     * 导出月消课明细
     */
    void exportStatisDecLesson(LessonParamDTO param);

    /**
     * 导出课表
     */
    void exportLessonData(LessonParamDTO param);

    /**
     * 上课提醒
     */
    void lessonNotice();

    /**
     * 课次数预警提醒
     */
    void lessonLessWarning();

    /**
     * 学生请假
     */
    Boolean studentLeaveApply(Long studentId, Long lessonId);

    /**
     * 修改课程签到消课课程
     */
    boolean changeCourseAtSign(Long lessonId, Long studentId, Long courseId);

    /**
     * 老师点评
     */
    boolean teachEvaluate(TeachEvaluateDTO dto, Long studentId);

    boolean addStudents(LessonStudentAddDTO dto, Long currentUserId);

    /**
    * 开启与关闭预约状态
    * */
    boolean handleBooking(List<Long> ids, Boolean state);

    /**
     * 学生预约
     */
    Boolean studentAppoint(Long lessonId, Long studentId);

    /**
     * 学生端是否可以签到逻辑
     */
    void studentCanSignLogic(List<LessonVO> records);

    /**
    * 批量修改只可修改增减天数、上课时间、教室；
    * */
    boolean updateLessonBatch(LessonSaveDTO dto);

    List<Map<String, Object>> getLessonNumEveryDay(LessonParamDTO param);


    /**
     * 检查老师签到是否超过时间限制 签到和补签的才验证
     * @param lesson 课程实体
     * @param state 要签到的状态
     * @param throwExp 是否抛出异常
     * @return boolean true表示可以签到 false表示不可以签到，throwExp 为 true时，不返回
     * */
    boolean checkTeacherSignLimit(Lesson lesson, SignStateEnum state, boolean throwExp);
}
