package com.hzb.erp.api.pc.course.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hzb.erp.api.pc.course.entity.CourseSection;

/**
 * <p> </p>
 *
 * @author Ryan 541720500@qq.com
 */
public interface CourseSectionService extends IService<CourseSection> {
    IPage<CourseSection> sectionPage(IPage<CourseSection> ipage, Long courseId);
}
