package com.hzb.erp.api.pc.lesson.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

/**
 * @author Ryan 541720500@qq.com
 * description
 */
@Data
public class LessonBatchSaveDTO extends LessonSaveDTO {
    private List<WeekSetting> weekSetting;

    private List<Long> studentIds;
    private Long courseId;

    private List<LocalDate> dateList;

    @ApiModelProperty(value = "排课类型 loop重复排课 freedom自由排课 vip一对一排课")
    private String type;

    @Data
    public static class WeekSetting {

        private Integer[] weekDays;

        private LocalTime startTime;

        private LocalTime endTime;

        private Integer decCount;

    }
}

