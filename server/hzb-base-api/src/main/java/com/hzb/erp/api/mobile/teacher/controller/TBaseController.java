package com.hzb.erp.api.mobile.teacher.controller;

import com.hzb.erp.api.base.service.StaffAuthService;
import com.hzb.erp.api.mobile.student.service.StudentAuthService;
import com.hzb.erp.api.pc.sys.service.CommonService;
import com.hzb.erp.common.entity.Staff;
import com.hzb.erp.common.pojo.StaffVO;
import com.hzb.erp.common.service.StaffService;
import com.hzb.erp.configuration.SystemConfig;
import com.hzb.erp.security.service.impl.StaffUserDetailsService;
import com.hzb.erp.sysservice.FileService;
import com.hzb.erp.sysservice.bo.UploadResultBO;
import com.hzb.erp.sysservice.bo.UploadValidateBO;
import com.hzb.erp.utils.JsonResponse;
import com.hzb.erp.utils.JsonResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.Map;

import static com.hzb.erp.constants.CommonConst.TEACHER_CENTER_URL_PREFIX;

/**
 * @author Ryan 541720500@qq.com
 * description
 */
@RestController
@RequestMapping(TEACHER_CENTER_URL_PREFIX + "/base")
@Api(value = "基础数据", tags = "基础数据")
public class TBaseController {

    @Autowired
    private CommonService commonService;

    @Autowired
    private SystemConfig systemConfig;

    @Autowired
    private FileService fileService;

    @Autowired
    private StaffService staffService;

    @Autowired
    private StaffUserDetailsService staffUserDetailsService;

    @ApiOperation("登录信息")
    @GetMapping("/getLoginStaff")
    public StaffVO getLoginStaff() {
        return StaffAuthService.getLoginStaff();
    }

    @ApiOperation("首页统计数据")
    @GetMapping("/getHomeCounts")
    public Map<String, Long> getHomeCounts() {
        return commonService.teacherCenterStatsCount(StaffAuthService.getCurrentUserId());
    }

    @PostMapping("/uploadAvatar")
    @ResponseBody
    public JsonResponse uploadAvatar(@RequestParam("file") MultipartFile file) throws FileUploadException {
        UploadValidateBO validateBO = new UploadValidateBO();
        validateBO.setMaxSize(systemConfig.getUploadImgMaxSize());
        validateBO.setIsImage(true);
        UploadResultBO uploadResult = fileService.upload(file, validateBO, null);

        Staff staff = staffService.getById(StaffAuthService.getCurrentUserId());
        staff.setHeadImg(uploadResult.getUrl());

        return staffService.updateById(staff) ?
                JsonResponseUtil.success() :
                JsonResponseUtil.error();
    }

    @ApiOperation("上传图片")
    @PostMapping("/upload")
    @ResponseBody
    public JsonResponse upload(@RequestParam("file") MultipartFile file) throws FileUploadException {
        UploadValidateBO validateBO = new UploadValidateBO();
        validateBO.setMaxSize(systemConfig.getUploadImgMaxSize());
        validateBO.setIsImage(true);
        return JsonResponseUtil.data(fileService.upload(file, validateBO, null));
    }

    @ApiOperation("保存wx登录信息表")
    @GetMapping("/storeWxAccessId")
    public void storeWxAccessId(@RequestParam(value = "wxAccessId") Long wxAccessId) {
        staffUserDetailsService.storeWxAccessId(StaffAuthService.getCurrentUserId(), wxAccessId);
    }
}
