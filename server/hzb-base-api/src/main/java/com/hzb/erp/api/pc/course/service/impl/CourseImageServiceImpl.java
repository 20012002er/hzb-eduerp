package com.hzb.erp.api.pc.course.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hzb.erp.api.pc.course.entity.Course;
import com.hzb.erp.api.pc.course.entity.CourseImage;
import com.hzb.erp.api.pc.course.mapper.CourseImageMapper;
import com.hzb.erp.api.pc.course.mapper.CourseMapper;
import com.hzb.erp.api.pc.course.service.CourseImageService;
import com.hzb.erp.api.pc.course.service.CourseService;
import org.springframework.stereotype.Service;

/**
 * <p> </p>
 *
 * @author Ryan 541720500@qq.com
 */
@Service
public class CourseImageServiceImpl extends ServiceImpl<CourseImageMapper, CourseImage> implements CourseImageService {
}
