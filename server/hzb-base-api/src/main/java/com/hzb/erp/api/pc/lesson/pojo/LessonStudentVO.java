package com.hzb.erp.api.pc.lesson.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hzb.erp.common.enums.SignStateEnum;
import com.hzb.erp.common.enums.SignTypeEnum;
import com.hzb.erp.api.pc.student.pojo.StudentVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
public class LessonStudentVO extends StudentVO {
    private Long id;

    private SignStateEnum signState;

    @ApiModelProperty(value = "实扣课次数")
    private Integer decLessonCount;

    private Boolean onTrail;

    private SignTypeEnum signType;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime signTime;

    private Long lessonId;
    private String lessonTitle;
    private String teacherName;
    private Long teacherId;

    private String courseName;

    private Integer score;
    private String evaluation;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime evaluateTime;
    private String evaluateTeacherName;
    private Integer countTeachEvaluation;

    @ApiModelProperty(value = "是否可以学评师")
    private Boolean canEvaluate;

    @ApiModelProperty(value = "上课日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    @ApiModelProperty(value = "开始时间")
    @JsonFormat(pattern = "HH:mm")
    private LocalTime startTime;

    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "HH:mm")
    private LocalTime endTime;

}
