package com.hzb.erp.api.pc.finance.pojo;

import com.hzb.erp.api.pc.finance.entity.OperationRecord;
import lombok.Data;

@Data
public class OperationRecordVO extends OperationRecord {
    private String creatorName;
    private String comName;
    private String dptName;
}
