package com.hzb.erp.api.pc.lesson.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

/**
 * @author Ryan 541720500@qq.com
 * description
 */
@Data
public class LessonSaveDTO {

    private Long id;
    private String title;

    @NotNull(message = "未选择老师")
    @Size(min = 1, message = "未选择老师")
    private List<Long> teacherIds;

    private List<Long> assistantIds;

    private Long classId;

    private Long roomId;

    private LocalDate date;

    private Integer decCount;

    private LocalTime startTime;

    private LocalTime endTime;

    @NotNull(message = "是否开放预约")
    private Boolean bookable;

    /**
     * 以下参数开启循环的时候会用到
     * */
    @ApiModelProperty(value = "是否循环")
    private Boolean loop;

    @ApiModelProperty(value = "循环结束日期")
    private LocalDate endDate;

    @ApiModelProperty(value = "是否跳过假期")
    private Boolean noholidy;

    @ApiModelProperty(value = "循环周几上课")
    private Integer[] weekDays;

    @ApiModelProperty(value = "批量修改的id列表")
    private Long[] updateIds;

    @ApiModelProperty(value = "增减天数")
    private Integer changeDays;

    @ApiModelProperty(value = "编辑后续课次")
    private Boolean batchUpdate;
}
