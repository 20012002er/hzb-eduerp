package com.hzb.erp.api.mobile.student.controller;

import com.hzb.erp.common.service.UserService;
import com.hzb.erp.constants.CommonConst;
import com.hzb.erp.security.Util.SecurityUtils;
import com.hzb.erp.sysservice.SmsManager;
import com.hzb.erp.sysservice.enums.SmsSceneType;
import com.hzb.erp.api.mobile.student.pojo.dto.ForgetPasswordDTO;
import com.hzb.erp.api.mobile.student.pojo.dto.UserRegisterDTO;
import com.hzb.erp.utils.CommonUtil;
import com.hzb.erp.utils.JsonResponse;
import com.hzb.erp.utils.JsonResponseUtil;
import com.hzb.erp.utils.RequestUtil;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.hzb.erp.constants.CommonConst.STUDENT_CENTER_URL_PREFIX;

/**
 * @author Ryan 541720500@qq.com
 * description
 */
@RestController
@RequestMapping(STUDENT_CENTER_URL_PREFIX + "/open")
@Api(value = "开放接口", tags = "开放接口")
public class SOpenController {

    @Autowired
    private UserService userService;

    @Autowired
    private SmsManager smsManager;

    @PostMapping("/register")
    @ResponseBody
    public JsonResponse register(@Valid @RequestBody UserRegisterDTO dto, BindingResult result) {
        CommonUtil.handleValidMessage(result);

        if (dto.getAgree() == null || !dto.getAgree()) {
            return JsonResponseUtil.error("未同意服务条件，无法注册");
        }

        if (!smsManager.valid(SmsSceneType.STUDENT_REGISTER.getCode(), dto.getMobile(), dto.getSmscode())) {
            return JsonResponseUtil.error("短信验证码错误");
        }

        if (userService.register(dto.getMobile(),
                SecurityUtils.passwordEncode(dto.getPassword()),
                dto.getName(),
                this.getAccessId(dto))
        ) {
            return JsonResponseUtil.success();
        } else {
            return JsonResponseUtil.error("注册失败");
        }
    }

    private String getAccessId(UserRegisterDTO dto) {
        if(StringUtils.isNotBlank(dto.getWxAccessId())) {
            return dto.getWxAccessId();
        }
        return RequestUtil.getCookieValue(CommonConst.WX_ACCESS_COOKIE);
    }

    @PostMapping("/forgetPwd")
    @ResponseBody
    public JsonResponse forgetPwd(@Valid @RequestBody ForgetPasswordDTO dto, BindingResult result) {
        CommonUtil.handleValidMessage(result);
        if (!smsManager.valid(SmsSceneType.STUDENT_FORGETPW.getCode(), dto.getMobile(), dto.getSmscode())) {
            return JsonResponseUtil.error("短信验证码错误");
        }
        if (userService.updatePwdByMob(dto.getMobile(), SecurityUtils.passwordEncode(dto.getPassword()))) {
            return JsonResponseUtil.success();
        } else {
            return JsonResponseUtil.error("注册失败");
        }
    }
}
