package com.hzb.erp.api.pc.lesson.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
* 老师课程数消课统计
* */
@Data
public class LessonCountDecreaseStatsVO {
    @ApiModelProperty(value = "老师ID")
    private Long teacherId;
    @ApiModelProperty(value = "学生ID")
    private Long studentId;
    @ApiModelProperty(value = "消课ID")
    private Long courseId;
    @ApiModelProperty(value = "老师姓名")
    private String teacherName;
    @ApiModelProperty(value = "学生姓名")
    private String studentName;
    @ApiModelProperty(value = "年")
    private Integer year;
    @ApiModelProperty(value = "月")
    private Integer month;
    @ApiModelProperty(value = "消课名称")
    private String courseName;
    @ApiModelProperty(value = "消课数")
    private Integer decCountSum;
    @ApiModelProperty(value = "单节课酬")
    private BigDecimal courseSalary;
    @ApiModelProperty(value = "课酬小计")
    private BigDecimal totalSalary;
}