package com.hzb.erp.api.pc.student.pojo;

import com.hzb.erp.common.enums.StudentCreditChangeTypeEnum;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class StudentCreditChangeParamDTO {
    @NotNull(message = "缺少参数")
    private Long studentId;
    @NotNull(message = "请输入积分")
    private Integer credit;
    private String remark;
    // 调整人
    private Long staffId;
    private StudentCreditChangeTypeEnum type;
}
