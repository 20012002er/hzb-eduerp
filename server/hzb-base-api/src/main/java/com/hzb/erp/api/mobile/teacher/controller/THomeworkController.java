package com.hzb.erp.api.mobile.teacher.controller;

import com.hzb.erp.annotation.Log;
import com.hzb.erp.annotation.PreventMultiSubmit;
import com.hzb.erp.api.pc.lesson.pojo.HomeworkParamDTO;
import com.hzb.erp.api.pc.lesson.pojo.HomeworkRecordCommentSaveDTO;
import com.hzb.erp.api.pc.lesson.pojo.HomeworkSaveDTO;
import com.hzb.erp.api.pc.lesson.pojo.HomeworkVO;
import com.hzb.erp.common.pojo.PaginationVO;
import com.hzb.erp.api.pc.lesson.service.HomeworkRecordService;
import com.hzb.erp.api.pc.lesson.service.HomeworkService;
import com.hzb.erp.api.base.service.StaffAuthService;
import com.hzb.erp.utils.CommonUtil;
import com.hzb.erp.utils.JsonResponse;
import com.hzb.erp.utils.JsonResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static com.hzb.erp.constants.CommonConst.TEACHER_CENTER_URL_PREFIX;

/**
 * @author Ryan 541720500@qq.com
 * description
 */
@RestController
@RequestMapping(TEACHER_CENTER_URL_PREFIX + "/homework")
@Api(value = "作业", tags = "作业")
public class THomeworkController {

    @Autowired
    private HomeworkService homeworkService;
    @Autowired
    private HomeworkRecordService homeworkRecordService;

    @ApiOperation("作业信息")
    @GetMapping("/info")
    public HomeworkVO info(@RequestParam("id") Long id) {
        return homeworkService.getInfo(id, null);
    }


    @ApiOperation("作业列表")
    @GetMapping("/list")
    public PaginationVO list(
            @RequestParam(value = "page", defaultValue = "") Integer page,
            @RequestParam(value = "pageSize", defaultValue = "30") Integer pageSize) {

        HomeworkParamDTO param = new HomeworkParamDTO();
        param.setPage(page);
        param.setPageSize(pageSize);
        param.setTeacherId(StaffAuthService.getCurrentUserId());

        return JsonResponseUtil.paginate(homeworkService.getList(param));
    }

    @ApiOperation("作业评价")
    @Log(description = "作业评价", type = "作业管理")
    @PostMapping("/comment")
    public JsonResponse comment(@Valid @RequestBody HomeworkRecordCommentSaveDTO dto, BindingResult result) {
        CommonUtil.handleValidMessage(result);
        if (homeworkRecordService.comment(dto, StaffAuthService.getCurrentUserId())) {
            return JsonResponseUtil.success();
        } else {
            return JsonResponseUtil.error("操作失败");
        }
    }

    @ApiOperation("创建和修改作业")
    @Log(description = "创建和修改作业", type = "作业管理")
    @PostMapping("/save")
    @PreventMultiSubmit
    public JsonResponse save(@Valid @RequestBody HomeworkSaveDTO dto, BindingResult result) {
        CommonUtil.handleValidMessage(result);

        StringBuilder content = new StringBuilder("<p>" + dto.getContent() + "</p>");
        List<String> imgList = dto.getImgList();

        if(!CollectionUtils.isEmpty(imgList)) {
            content.append("<p>");
            for (String img: imgList) {
                content.append("<img src='").append(img).append("' style='width: 100%;'/>");
            }
            content.append("</p>");
        }
        dto.setContent(content.toString());
        homeworkService.saveOrUpdateByDTO(dto);
        return JsonResponseUtil.success();
    }

}
