package com.hzb.erp.api.pc.lesson.pojo;

import com.hzb.erp.common.pojo.PaginateDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LessonEvaluateSaveDTO extends PaginateDTO {

    @NotNull(message = "缺少主键")
    private Long id;

    @NotNull(message = "缺少评分")
    @ApiModelProperty(value = "评分/赠送积分")
    private Integer score;

    @ApiModelProperty(value = "评价内容")
    private String evaluation;
}
