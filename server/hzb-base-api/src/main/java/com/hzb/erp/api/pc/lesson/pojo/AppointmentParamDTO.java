package com.hzb.erp.api.pc.lesson.pojo;

import com.hzb.erp.common.pojo.PaginateDTO;
import lombok.Data;

import java.time.LocalDate;

@Data
public class AppointmentParamDTO extends PaginateDTO {
    private Long studentId;
    private Long courseId;
    private Long[] courseIds;
    private LocalDate endDate;
    private LocalDate startDate;
}
