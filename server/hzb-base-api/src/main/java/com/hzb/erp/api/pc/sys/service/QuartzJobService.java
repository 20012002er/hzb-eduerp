package com.hzb.erp.api.pc.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzb.erp.api.pc.sys.entity.Notice;
import com.hzb.erp.quartz.entity.QuartzJob;

import java.util.List;

/**
 * <p> </p>
 *
 * @author Ryan 541720500@qq.com
 */
public interface QuartzJobService  extends IService<QuartzJob> {
    List<QuartzJob> getList();
    QuartzJob getByJobName(String jobName);
}
