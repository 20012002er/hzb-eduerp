package com.hzb.erp.api.pc.lesson.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hzb.erp.api.pc.clazz.entity.Clazz;
import com.hzb.erp.api.pc.lesson.entity.Lesson;
import com.hzb.erp.api.pc.lesson.entity.LessonStudent;
import com.hzb.erp.api.pc.lesson.pojo.LessonStudentCountsVO;
import com.hzb.erp.api.pc.student.entity.Student;
import com.hzb.erp.api.pc.student.pojo.StudentCourseVO;
import com.hzb.erp.common.enums.SignStateEnum;
import com.hzb.erp.common.enums.SignTypeEnum;
import com.hzb.erp.api.pc.lesson.pojo.LessonEvaluateSaveDTO;
import com.hzb.erp.api.pc.lesson.pojo.LessonStudentParamDTO;
import com.hzb.erp.api.pc.lesson.pojo.LessonStudentVO;

import java.util.List;

/**
 * <p>
 * 课时学员关联表 服务类
 * </p>
 *
 * @author Ryan
 */
public interface LessonStudentService extends IService<LessonStudent> {

    /**
     * 带分页的列表
     *
     * @param param 参数
     */
    IPage<LessonStudentVO> getList(LessonStudentParamDTO param);

    List<LessonStudentVO> getAll(LessonStudentParamDTO param);

    /**
    * 通过课时和学生id获取 应该只有一条数据
    * */
    LessonStudent getByLessonIdAndStudentId(Long lessonId, Long studentId);

    List<LessonStudent> listByStudentId(Long studentId);

    /**
     * 点名和签到时用于增加签到记录,并扣减响应的课时.
     * @param lessonId 课时id
     * @param studentId 学生id
     * @param lesson 课程id
     * @param state 签到状态
     * @param teacherId 操作老师id
     */
    LessonStudent addRecord(Long lessonId, Long studentId, Lesson lesson, SignStateEnum state, SignTypeEnum type, Long teacherId);

    /**
     * 只添加课时和学生关系记录
     * @param lessonId 课时id
     * @param student 学生
     * @param state 签到状态
     */
    LessonStudent addRecord(Long lessonId, Student student, SignStateEnum state);

    /**
     * 通过班级添加学生的上课记录，不会重复，只生成未开始课程的
     * @param clazz 班级
     */
    List<LessonStudent> addRecord(Clazz clazz);

    /**
     * 通过班级和学生添加上课记录, 不会重复，只生成未开始课程的
     * @param clazz 班级
     * @param student 学生
     */
    List<LessonStudent> addRecord(Clazz clazz, Student student);

    /**
     * 判断是否需要扣课时
     */
    Boolean calDecLessonCountByState(LessonStudent ls, SignStateEnum state);

    /**
    * 课后点评
    * */
    boolean evaluation(LessonEvaluateSaveDTO dto, Long teacherId);

    boolean rollbackCourseNum(List<Long> ids, Long teacherId);

    LessonStudentCountsVO loadSignCounts(LessonStudentParamDTO param);

    /**
    * 统计签到数字赋值
    * */
    void assignSignCounts(List<StudentCourseVO> list);

    /**
     * 删除上课记录
     */
    Boolean deleteLessonStudent(List<Long> ids);

    boolean changeLessonCount(LessonStudent dto);
}
