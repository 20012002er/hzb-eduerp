package com.hzb.erp.api.mobile.student.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hzb.erp.annotation.Log;
import com.hzb.erp.api.mobile.student.pojo.dto.StudentLeaveDTO;
import com.hzb.erp.api.mobile.student.service.StudentAuthService;
import com.hzb.erp.api.pc.lesson.pojo.*;
import com.hzb.erp.api.pc.lesson.service.LessonService;
import com.hzb.erp.api.pc.lesson.service.LessonStudentService;
import com.hzb.erp.api.pc.student.entity.Student;
import com.hzb.erp.api.pc.student.service.StudentLeaveService;
import com.hzb.erp.api.pc.student.service.StudentService;
import com.hzb.erp.common.pojo.PaginationVO;
import com.hzb.erp.utils.CommonUtil;
import com.hzb.erp.utils.DateTool;
import com.hzb.erp.utils.JsonResponse;
import com.hzb.erp.utils.JsonResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static com.hzb.erp.constants.CommonConst.STUDENT_CENTER_URL_PREFIX;

/**
 * @author Ryan 541720500@qq.com
 * description
 */
@RestController
@RequestMapping(STUDENT_CENTER_URL_PREFIX + "/lesson")
@Api(value = "课次", tags = "课次")
public class SLessonController {

    @Autowired
    private LessonService lessonService;

    @Resource
    private LessonStudentService lessonStudentService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentLeaveService studentLeaveService;

    @ApiOperation("课次信息")
    @GetMapping("/info")
    public LessonVO info(@RequestParam(value = "id") Long id) {
        return lessonService.getInfo(id);
    }

    @ApiOperation("课表")
    @GetMapping("/list")
    public Object list(@RequestParam(value = "page", defaultValue = "") Integer page, // 若为null则是查全部
                       @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize,
                       @RequestParam(value = "bookable", defaultValue = "false") Boolean bookable, // 是否是可预约列表
                       @RequestParam(value = "date", defaultValue = "") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        LessonParamDTO param = buildParam(page, pageSize, date, bookable);
        if (param == null) {
            return null;
        }
        Object result;
        if(page != null && page > 0) {
            IPage<LessonVO> list = lessonService.getList(param);
            lessonService.studentCanSignLogic(list.getRecords());
            result = JsonResponseUtil.paginate(list);
        } else {
            List<LessonVO> list = lessonService.getAll(param);
            lessonService.studentCanSignLogic(list);
            result = list;
        }
        return result;
    }

    private LessonParamDTO buildParam(Integer page, Integer pageSize, LocalDate date, Boolean bookable) {
        LessonParamDTO param = new LessonParamDTO();
        param.setPage(page);
        param.setPageSize(pageSize);
        param.setBookable(bookable);
        Student student = StudentAuthService.getCurrentStudent();
        if (student == null) {
            return null;
        }
        Long studentId = student.getId();
        param.setStudentId(studentId);
        param.setDate(date);
        return param;
    }

    @ApiOperation("日课表数量统计")
    @GetMapping("/lessonCountEveryDay")
    public List<Map<String, Object>> lessonCountEveryDay(@RequestParam(value = "date", defaultValue = "") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        LessonParamDTO param = new LessonParamDTO();
        param.setStartDate(DateTool.firstDayOfMonth(date));
        param.setEndDate(DateTool.lastDayOfMonth(date));
        Student student = StudentAuthService.getCurrentStudent();
        if (student == null) {
            return null;
        }
        Long studentId = student.getId();
        param.setStudentId(studentId);
        return lessonService.getLessonNumEveryDay(param);
    }

    @ApiOperation("签到记录")
    @GetMapping("/signRecord")
    public PaginationVO rollCallRecord(
            @RequestParam(value = "page", defaultValue = "") Integer page,
            @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize,
            @RequestParam(value = "courseId", defaultValue = "") Long courseId,
            @RequestParam(value = "signState", defaultValue = "0") Integer signState
            ) {

        LessonStudentParamDTO param = new LessonStudentParamDTO();
        param.setPage(page);
        param.setPageSize(pageSize);
        Student student = StudentAuthService.getCurrentStudent();
        if (student == null) {
            return null;
        }
        Long studentId = student.getId();
        param.setStudentId(studentId);
        param.setCourseId(courseId);
        param.setSignState(signState);
        param.setStudentSide(true);
        return JsonResponseUtil.paginate(lessonStudentService.getList(param));
    }

    @ApiOperation("签到")
    @PostMapping("/sign/{lessonId}")
    public JsonResponse sign(@PathVariable("lessonId") Long lessonId) {
        Student student = StudentAuthService.getCurrentStudent();
        if (student == null) {
            return JsonResponseUtil.error("请先添加学生");
        }
        Long studentId = student.getId();
        if (lessonService.studentSign(lessonId, studentId, lessonService.getSignStateByNow(lessonId))) {
            return JsonResponseUtil.success("签到完成");
        } else {
            return JsonResponseUtil.error("签到失败");
        }
    }

    @ApiOperation("点评记录")
    @GetMapping("/evaluateLog")
    public PaginationVO evaluateLog(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                    @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize) {
        Student student = StudentAuthService.getCurrentStudent();
        if (student == null) {
            return null;
        }
        Long studentId = student.getId();
        LessonStudentParamDTO param = new LessonStudentParamDTO();
        param.setPage(page);
        param.setPageSize(pageSize);
        param.setStudentId(studentId);
        param.setOnlyEvaluate(true);
        param.setStudentSide(true);
        student.setRedpointEvaluate(LocalDateTime.now());
        studentService.updateById(student);
        return JsonResponseUtil.paginate(lessonStudentService.getList(param));
    }

    @ApiOperation("授课评价")
    @Log(description = "授课评价", type = "学生端", isStaff = false)
    @PostMapping("/teachEvaluate")
    public JsonResponse teachEvaluate(@Valid @RequestBody TeachEvaluateDTO dto, BindingResult result) {
        CommonUtil.handleValidMessage(result);
        Student student = StudentAuthService.getCurrentStudent();
        if (student == null) {
            return JsonResponseUtil.error("请先添加学生");
        }
        if (lessonService.teachEvaluate(dto, student.getId())) {
            return JsonResponseUtil.success("已添加");
        } else {
            return JsonResponseUtil.error();
        }
    }

    @ApiOperation("预约")
    @Log(description = "预约", type = "学生端", isStaff = false)
    @PostMapping("/appoint/{lessonId}")
    public JsonResponse appoint(@PathVariable("lessonId") Long lessonId) {
        Student student = StudentAuthService.getCurrentStudent();
        if (student == null) {
            return JsonResponseUtil.error("请先添加学生");
        }
        Long studentId = student.getId();
        if (lessonService.studentAppoint(lessonId, studentId)) {
            return JsonResponseUtil.success("预约完成");
        } else {
            return JsonResponseUtil.error("预约失败");
        }
    }

    @ApiOperation("请假")
    @Log(description = "请假", type = "学生端", isStaff = false)
    @PostMapping("/leave")
    public JsonResponse leave(@Valid @RequestBody StudentLeaveDTO dto, BindingResult result) {
        CommonUtil.handleValidMessage(result);
        Student student = StudentAuthService.getCurrentStudent();
        if (student == null) {
            return JsonResponseUtil.error("请先添加学生");
        }
        dto.setStudentId(student.getId());
        if (studentLeaveService.handleLeave(dto)) {
            return JsonResponseUtil.success("请假完成");
        } else {
            return JsonResponseUtil.error("请假失败");
        }
    }

    @ApiOperation("加载签到数据")
    @GetMapping("/loadSignCounts")
    public LessonStudentCountsVO loadSignCounts(@RequestParam(value = "courseId", defaultValue = "") Long courseId) {
        Student student = StudentAuthService.getCurrentStudent();
        if (student == null) {
            return null;
        }
        LessonStudentParamDTO param = new LessonStudentParamDTO();
        param.setStudentId(student.getId());
        param.setCourseId(courseId);
        return lessonStudentService.loadSignCounts(param);
    }
}
