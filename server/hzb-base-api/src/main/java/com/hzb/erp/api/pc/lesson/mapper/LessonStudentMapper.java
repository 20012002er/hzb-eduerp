package com.hzb.erp.api.pc.lesson.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hzb.erp.annotation.DataScoped;
import com.hzb.erp.api.pc.clazz.entity.ClassStudent;
import com.hzb.erp.api.pc.lesson.entity.Lesson;
import com.hzb.erp.api.pc.lesson.entity.LessonStudent;
import com.hzb.erp.api.pc.lesson.pojo.LessonStudentCountsVO;
import com.hzb.erp.api.pc.lesson.pojo.LessonStudentParamDTO;
import com.hzb.erp.api.pc.lesson.pojo.LessonStudentVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 课时学员关联表 Mapper 接口
 * </p>
 *
 * @author Ryan
 */
@Repository
@Mapper
public interface LessonStudentMapper extends BaseMapper<LessonStudent> {

    @DataScoped
    IPage<LessonStudentVO> getList(Page<Object> objectPage, LessonStudentParamDTO param);

    @DataScoped
    List<LessonStudentVO> getList(@Param("param") LessonStudentParamDTO param);

    /**
     * 查找要消费的课程id
     */
    Long getConsumeCourseId(@Param("lessonId") Long lessonId, @Param("studentId") Long studentId);

    List<Long> getStudentIds(Long id);

    /**
     * 删除学生记录，包含未签到的
     */
    Boolean removeByStudentIdAndClassId(@Param("studentId") Long studentId, @Param("classId") Long classId);

    LessonStudent getByLessonIdAndStudentId( @Param("LessonId") Long lessonId, @Param("studentId") Long studentId);

    /**
     * 按班级查询将来的课，用于生成学生课时记录
     */
    List<Lesson> listFutureLessonByClassId(@Param("classId") Long classId);

    /**
    * 加载课程签到情况统计数据
    * */
    LessonStudentCountsVO loadSignCounts(@Param("param") LessonStudentParamDTO param);

    /**
    *  对未开始的课程的上课记录里的消课是否使用该消课项
    * */
    void updateCourseIdByClassStudentForFuture(ClassStudent item);
}
