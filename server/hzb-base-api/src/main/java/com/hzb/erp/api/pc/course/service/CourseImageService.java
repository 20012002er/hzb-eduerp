package com.hzb.erp.api.pc.course.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzb.erp.api.pc.course.entity.CourseImage;

/**
 * <p> </p>
 *
 * @author Ryan 541720500@qq.com
 */
public interface CourseImageService extends IService<CourseImage> {
}
