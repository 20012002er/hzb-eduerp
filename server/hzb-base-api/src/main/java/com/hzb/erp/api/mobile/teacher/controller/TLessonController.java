package com.hzb.erp.api.mobile.teacher.controller;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.hzb.erp.annotation.Log;
import com.hzb.erp.api.base.service.StaffAuthService;
import com.hzb.erp.api.mobile.teacher.service.TLessonService;
import com.hzb.erp.api.pc.clazz.pojo.ClassStudentParamDTO;
import com.hzb.erp.api.pc.clazz.pojo.ClassStudentSignVO;
import com.hzb.erp.api.pc.clazz.service.ClassStudentService;
import com.hzb.erp.api.pc.lesson.pojo.LessonEvaluateSaveDTO;
import com.hzb.erp.api.pc.lesson.pojo.LessonParamDTO;
import com.hzb.erp.api.pc.lesson.pojo.LessonStudentParamDTO;
import com.hzb.erp.api.pc.lesson.pojo.LessonStudentVO;
import com.hzb.erp.api.pc.lesson.service.LessonService;
import com.hzb.erp.api.pc.lesson.service.LessonStudentService;
import com.hzb.erp.common.pojo.PaginationVO;
import com.hzb.erp.common.service.SettingService;
import com.hzb.erp.sysservice.enums.SettingNameEnum;
import com.hzb.erp.utils.CommonUtil;
import com.hzb.erp.utils.DateTool;
import com.hzb.erp.utils.JsonResponse;
import com.hzb.erp.utils.JsonResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static com.hzb.erp.constants.CommonConst.TEACHER_CENTER_URL_PREFIX;

/**
 * @author Ryan 541720500@qq.com
 * description
 */
@RestController
@RequestMapping(TEACHER_CENTER_URL_PREFIX + "/lesson")
@Api(value = "课程课次", tags = "课程课次")
public class TLessonController {

    @Autowired
    private LessonService lessonService;

    @Autowired
    private TLessonService tLessonService;

    @Autowired
    private LessonStudentService lessonStudentService;

    @Autowired
    private ClassStudentService classStudentService;

    @Autowired
    private SettingService settingService;

    @ApiOperation("课表")
    @GetMapping("/list")
    public Object list(@RequestParam(value = "page", defaultValue = "") Integer page, // 若为null则是查全部
                       @RequestParam(value = "pageSize", defaultValue = "30") Integer pageSize,
                       @RequestParam(value = "date", defaultValue = "") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {

        LessonParamDTO param = new LessonParamDTO();
        param.setDate(date);
        param.setPage(page);
        param.setPageSize(pageSize);
        param.setTeacherId(StaffAuthService.getCurrentUserId());

        return page != null && page > 0 ?
                JsonResponseUtil.paginate(lessonService.getList(param)) :
                lessonService.getAll(param);
    }

    @ApiOperation("班级学员签到情况")
    @GetMapping("/classStudents")
    public List<ClassStudentSignVO> classStudents(
            @RequestParam(value = "lessonId", defaultValue = "") Long lessonId,
            @RequestParam(value = "unsigned", defaultValue = "") Boolean unsigned) {
        ClassStudentParamDTO param = new ClassStudentParamDTO();
        param.setLessonId(lessonId);
        param.setUnsigned(unsigned);
        List<ClassStudentSignVO> records = classStudentService.signRecord(param);

        Boolean marker = settingService.boolValue(SettingNameEnum.STUDENT_MOBILE_MARKER_SWITCH.getCode());
        if(BooleanUtils.isNotFalse(marker) && CollectionUtils.isNotEmpty(records)) {
            for (ClassStudentSignVO vo: records) {
                vo.setMobile(CommonUtil.markMobile(vo.getMobile()));
            }
        }

        return records;
    }

    @ApiOperation("点名记录")
    @GetMapping("/rollCallRecord")
    public PaginationVO rollCallRecord(
            @RequestParam(value = "page", defaultValue = "") Integer page,
            @RequestParam(value = "pageSize", defaultValue = "30") Integer pageSize) {

        LessonStudentParamDTO param = new LessonStudentParamDTO();
        param.setPage(page);
        param.setPageSize(pageSize);
        param.setTeacherId(StaffAuthService.getCurrentUserId());
        return JsonResponseUtil.paginate(lessonStudentService.getList(param));
    }

    @ApiOperation("日课表数量统计")
    @GetMapping("/lessonCountEveryDay")
    public List<Map<String, Object>> lessonCountEveryDay(@RequestParam(value = "date", defaultValue = "") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        LessonParamDTO param = new LessonParamDTO();
        param.setStartDate(DateTool.firstDayOfMonth(date));
        param.setEndDate(DateTool.lastDayOfMonth(date));

        return tLessonService.getLessonNumEveryDay(param);
    }

    @ApiOperation("学生上课点评记录")
    @GetMapping("/students")
    public List<LessonStudentVO> students(@RequestParam(value = "lessonId") Long lessonId) {
        LessonStudentParamDTO param = new LessonStudentParamDTO();
        param.setLessonId(lessonId);
        return lessonStudentService.getAll(param);
    }

    @ApiOperation("点评记录")
    @GetMapping("/evaluateLog")
    public PaginationVO evaluateLog(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                    @RequestParam(value = "pageSize", defaultValue = "30") Integer pageSize) {
        LessonStudentParamDTO param = new LessonStudentParamDTO();
        param.setPage(page);
        param.setPageSize(pageSize);
        param.setEvaluateTeacherId(StaffAuthService.getCurrentUserId());
        return JsonResponseUtil.paginate(lessonStudentService.getList(param));
    }

    @ApiOperation("课后点评")
    @Log(description = "课后点评", type = "课次管理")
    @PostMapping("/evaluation")
    public JsonResponse evaluation(@Valid @RequestBody LessonEvaluateSaveDTO dto, BindingResult result) {
        CommonUtil.handleValidMessage(result);
        if (lessonStudentService.evaluation(dto, StaffAuthService.getCurrentUserId())) {
            return JsonResponseUtil.success();
        } else {
            return JsonResponseUtil.error("操作失败");
        }
    }
}
