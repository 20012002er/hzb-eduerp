package com.hzb.erp.api.pc.sys.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hzb.erp.api.pc.finance.mapper.OperationRecordMapper;
import com.hzb.erp.api.pc.finance.pojo.OperationParamDTO;
import com.hzb.erp.api.pc.finance.pojo.OperationRecordVO;
import com.hzb.erp.api.pc.sys.entity.Region;
import com.hzb.erp.api.pc.sys.mapper.CommonMapper;
import com.hzb.erp.api.pc.sys.mapper.RegionMapper;
import com.hzb.erp.api.pc.sys.mapper.StatsMapper;
import com.hzb.erp.api.pc.sys.service.CommonService;
import com.hzb.erp.common.pojo.AutocompleteBuilderVO;
import com.hzb.erp.common.pojo.SelectBuilderVO;
import com.hzb.erp.quartz.mapper.QuartzJobMapper;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * <p> </p>
 *
 * @author Ryan 541720500@qq.com
 */
@Service
@AllArgsConstructor
public class CommonServiceImpl implements CommonService {
    private final CommonMapper commonMapper;
    private final StatsMapper statsMapper;
    private final OperationRecordMapper operationRecordMapper;
    private final RegionMapper regionMapper;
    @Override
    public List<SelectBuilderVO> selectBuilder(String code) {
        return commonMapper.selectBuilder(code);
    }

    @Override
    public String loadSelectorLabel(String code, String[] ids) {
        return commonMapper.loadSelectorLabel(code, ids);
    }

    @Override
    public List<SelectBuilderVO> dictBuilder(String code) {
        return commonMapper.dictBuilder(code);
    }

    @Override
    public List<AutocompleteBuilderVO> aotucomplateBuilder(String code, String keyword) {
        return commonMapper.aotucomplateBuilder(code, keyword);
    }

    @Override
    public List<Map<String, Object>> newStudentCounts(LocalDate startDate, LocalDate endDate) {
        return statsMapper.newStudentCounts(startDate, endDate);
    }

    @Override
    public List<Map<String, Object>> courseSalesByDate(LocalDate startDate, LocalDate endDate) {
        return statsMapper.courseSalesByDate(startDate, endDate);
    }

    @Override
    public List<Map<String, Object>> courseSalesTotal(LocalDate startDate, LocalDate endDate) {
        return statsMapper.courseSalesTotal(startDate, endDate);
    }

    @Override
    public List<Map<String, Object>> teacherLessonTotal(LocalDate startDate, LocalDate endDate) {
        return statsMapper.teacherLessonTotal(startDate, endDate);
    }

    @Override
    public List<Map<String, Object>> studentLessonCounts(LocalDate startDate, LocalDate endDate) {
        return statsMapper.studentLessonCounts(startDate, endDate);
    }

    @Override
    public Map<String, Object> dashboardCounts() {
        return statsMapper.dashboardCounts();
    }

    @Override
    public List<Map<String, Object>> funnelData() {
        return statsMapper.funnelData();
    }

    @Override
    public List<Map<String, Object>> studentAgeStats() {
        return statsMapper.studentAgeStats();
    }

    @Override
    public List<Map<String, Object>> teachEvaluationStatis(Long teacherId, LocalDate startDate, LocalDate endDate) {
        return statsMapper.teachEvaluationStatis(teacherId, startDate, endDate);
    }

    @Override
    public IPage<OperationRecordVO> getList(OperationParamDTO param) {
        return operationRecordMapper.getList(new Page<>(param.getPage(), param.getPageSize()), param);
    }

    @Override
    public List<Region> listRegionByPid(Integer pid) {
        return regionMapper.getByPid(pid);
    }

    @Override
    public Map<String, Long> teacherCenterStatsCount(Long teacherId) {
        return commonMapper.teacherCenterStatsCount(teacherId);
    }
}
