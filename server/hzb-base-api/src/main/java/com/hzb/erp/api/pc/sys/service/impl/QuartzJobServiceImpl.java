package com.hzb.erp.api.pc.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hzb.erp.api.pc.sys.service.QuartzJobService;
import com.hzb.erp.quartz.entity.QuartzJob;
import com.hzb.erp.quartz.mapper.QuartzJobMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p> </p>
 *
 * @author Ryan 541720500@qq.com
 */
@Service
@AllArgsConstructor
public class QuartzJobServiceImpl extends ServiceImpl<QuartzJobMapper, QuartzJob> implements QuartzJobService {

    @Override
    public List<QuartzJob> getList() {
        return this.baseMapper.getList();
    }

    @Override
    public QuartzJob getByJobName(String jobName) {
        QueryWrapper<QuartzJob> qw = new QueryWrapper<>();
        qw.eq("job_name", jobName);
        return this.baseMapper.selectOne(qw);
    }
}
