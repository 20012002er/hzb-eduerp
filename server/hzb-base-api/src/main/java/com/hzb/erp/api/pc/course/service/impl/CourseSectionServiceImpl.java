package com.hzb.erp.api.pc.course.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hzb.erp.api.pc.course.entity.CourseSection;
import com.hzb.erp.api.pc.course.mapper.CourseSectionMapper;
import com.hzb.erp.api.pc.course.service.CourseSectionService;
import org.springframework.stereotype.Service;

/**
 * <p> </p>
 *
 * @author Ryan 541720500@qq.com
 */
@Service
public class CourseSectionServiceImpl extends ServiceImpl<CourseSectionMapper, CourseSection> implements CourseSectionService {
    @Override
    public IPage<CourseSection> sectionPage(IPage<CourseSection> ipage, Long courseId) {
        QueryWrapper<CourseSection> qw = new QueryWrapper<>();
        qw.eq("course_id", courseId);
        return this.baseMapper.selectPage(ipage, qw);
    }
}
