package com.hzb.erp.api.pc.lesson.pojo;

import com.hzb.erp.common.pojo.PaginateDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;

/*
 * 课次学员关系表DTO
 * */
@Data
public class LessonStudentParamDTO extends PaginateDTO {
    private String keyword;
    private Long subjectId;
    private Long evaluateTeacherId;
    private Long teacherId;
    private Long lessonId;
    private Long studentId;
    private Long courseId;
    private Integer signState;
    private LocalDate startDate;
    private LocalDate endDate;
    private Boolean onlyEvaluate;

    @ApiModelProperty(value = "来自学生端的查询")
    private Boolean studentSide;
}
