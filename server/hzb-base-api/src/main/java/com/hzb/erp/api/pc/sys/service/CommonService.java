package com.hzb.erp.api.pc.sys.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hzb.erp.api.pc.finance.pojo.OperationParamDTO;
import com.hzb.erp.api.pc.finance.pojo.OperationRecordVO;
import com.hzb.erp.api.pc.sys.entity.Region;
import com.hzb.erp.common.pojo.AutocompleteBuilderVO;
import com.hzb.erp.common.pojo.SelectBuilderVO;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * <p> 通用服务类 </p>
 *
 * @author Ryan 541720500@qq.com
 */
public interface CommonService {

    List<SelectBuilderVO> selectBuilder(String code);

    String loadSelectorLabel(String code, String[] ids);

    List<SelectBuilderVO> dictBuilder(String code);

    List<AutocompleteBuilderVO> aotucomplateBuilder(String code, String keyword);

    List<Map<String, Object>> newStudentCounts(LocalDate startDate, LocalDate endDate);

    List<Map<String, Object>> courseSalesByDate(LocalDate startDate, LocalDate endDate);

    List<Map<String, Object>> courseSalesTotal(LocalDate startDate, LocalDate endDate);

    List<Map<String, Object>> teacherLessonTotal(LocalDate startDate, LocalDate endDate);

    List<Map<String, Object>> studentLessonCounts(LocalDate startDate, LocalDate endDate);

    Map<String, Object> dashboardCounts();

    List<Map<String, Object>> funnelData();

    List<Map<String, Object>> studentAgeStats();

    List<Map<String, Object>> teachEvaluationStatis(Long teacherId, LocalDate startDate, LocalDate endDate);

    IPage<OperationRecordVO> getList(OperationParamDTO param);

    List<Region> listRegionByPid(Integer pid);

    Map<String, Long> teacherCenterStatsCount(Long teacherId);

}
