package com.hzb.erp.api.pc.lesson.pojo;

import com.hzb.erp.common.pojo.PaginateDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;

/**
 * 课程签到情况统计数据
 * */
@Data
public class LessonStudentCountsVO extends PaginateDTO {
    @ApiModelProperty(value = "课程名称")
    private String courseName;
    @ApiModelProperty(value = "课程名称")
    private Long lessonId;
    @ApiModelProperty(value = "总记录")
    private int countTotal;
    @ApiModelProperty(value = "签到数")
    private int countSign;
    @ApiModelProperty(value = "请假数")
    private int countLeave;
    @ApiModelProperty(value = "旷课数")
    private int countAbsent;
    @ApiModelProperty(value = "补签数")
    private int countLate;
}
