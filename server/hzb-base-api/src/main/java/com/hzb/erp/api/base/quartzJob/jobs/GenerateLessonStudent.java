package com.hzb.erp.api.base.quartzJob.jobs;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.hzb.erp.api.pc.clazz.entity.ClassStudent;
import com.hzb.erp.api.pc.clazz.entity.Clazz;
import com.hzb.erp.api.pc.clazz.mapper.ClassStudentMapper;
import com.hzb.erp.api.pc.clazz.mapper.ClazzMapper;
import com.hzb.erp.api.pc.lesson.service.LessonStudentService;
import com.hzb.erp.api.pc.student.entity.Student;
import com.hzb.erp.api.pc.student.mapper.StudentMapper;
import com.hzb.erp.configuration.SystemConfig;
import lombok.SneakyThrows;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p> 定时生成班级学生的上课记录 </p>
 *
 * @author Ryan 541720500@qq.com
 */
public class GenerateLessonStudent extends QuartzJobBean {

    @Resource
    private ClassStudentMapper classStudentMapper;
    @Resource
    private ClazzMapper clazzMapper;

    @Resource
    private StudentMapper studentMapper;

    @Resource
    private SystemConfig systemConfig;
    @Resource
    private LessonStudentService lessonStudentService;


    @SneakyThrows
    @Override
    protected void executeInternal(@NotNull JobExecutionContext jobExecutionContext) throws JobExecutionException {
        List<ClassStudent> listNoLesson =  classStudentMapper.listNoLessonStudent();
        if(CollectionUtils.isNotEmpty(listNoLesson)) {
            for(ClassStudent cs: listNoLesson) {
                Clazz clazz = clazzMapper.selectById(cs.getClassId());
                Student student = studentMapper.selectById(cs.getStudentId());
                if(clazz!=null&& student!=null) {
                    lessonStudentService.addRecord(clazz, student);
                }
            }
        }
    }
}
