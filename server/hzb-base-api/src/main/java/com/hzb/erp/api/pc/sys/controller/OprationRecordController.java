package com.hzb.erp.api.pc.sys.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hzb.erp.api.pc.finance.pojo.OperationParamDTO;
import com.hzb.erp.api.pc.sys.service.CommonService;
import com.hzb.erp.common.pojo.PaginationVO;
import com.hzb.erp.utils.JsonResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 审核日志
 * </p>
 *
 * @author 541720500@qq.com
 */
@RestController
@RequestMapping("/common/oprationRecord")
@Api(value = "审核日志", tags = "审核日志")
public class OprationRecordController {
    @Autowired
    private CommonService commonService;

    @ApiOperation("审核日志")
    @GetMapping("/list")
    public PaginationVO list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                             @RequestParam(value = "pageSize", defaultValue = "30") Integer pageSize,
                             @RequestParam(value = "type", defaultValue = "") String type,
                             @RequestParam(value = "itemId") Long itemId) {
        OperationParamDTO dto = new OperationParamDTO();
        dto.setType(type);
        dto.setItemId(itemId);
        dto.setPage(page);
        dto.setPageSize(pageSize);
        return JsonResponseUtil.paginate(commonService.getList(dto));
    }

}
