user
以下相关的有school_id
student
student_leave

shop_order_refund
material_record
materail
grade
finance_record
lesson_schedule
lesson
class_room
credit_mall
credit_exchange
class
appointment



ALTER TABLE `student_credit_log` ADD COLUMN `org_id` bigint(20) NULL COMMENT '所属机构ID';
ALTER TABLE `homework` ADD COLUMN `org_id` bigint(20) NULL COMMENT '所属机构ID';
ALTER TABLE `student_course` ADD COLUMN `org_id` bigint(20) NULL COMMENT '所属机构ID';
ALTER TABLE `shop_order` ADD COLUMN `org_id` bigint(20) NULL COMMENT '所属机构ID';
ALTER TABLE `operation_record` ADD COLUMN `org_id` bigint(20) NULL COMMENT '所属机构ID';
ALTER TABLE `sys_log` ADD COLUMN `org_id` bigint(20) NULL COMMENT '所属机构ID';
ALTER TABLE `staff` ADD COLUMN `org_id` bigint(20) NULL COMMENT '所属机构ID';
ALTER TABLE `course` ADD COLUMN `org_id` bigint(20) NULL COMMENT '所属机构ID';
ALTER TABLE `contact_record` ADD COLUMN `org_id` bigint(20) NULL COMMENT '所属机构ID';
ALTER TABLE `cashout` ADD COLUMN `org_id` bigint(20) NULL COMMENT '所属机构ID';